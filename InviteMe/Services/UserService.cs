﻿using InviteMe.Entities;
using InviteMe.Helpers;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InviteMe.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);

        OTP GenrateOtp(OTP otp);
        OTP VerifyOtp(OTP otp);
        User GetByMobile(string mobile);

        IEnumerable<User> GetAll();
        User GetById(int id);
        User Create(User user);
        void Update(User user, string password = null);
        void Delete(int id);
    }

    public class UserService : IUserService
    {
        private DataContext _context;
        private readonly AppSettings _appSettings;

        public UserService(DataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _context.Users.SingleOrDefault(x => x.Mobile == username);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            //if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            //    return null;

            // authentication successful
            return user;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }

        public User GetById(int id)
        {
            return _context.Users.Find(id);
        }

        public User GetByMobile(string mobile)
        {
            return _context.Users.Where(i => i.Mobile == mobile).FirstOrDefault();
        }

        public OTP VerifyOtp(OTP otp)
        {
            var otpData = _context.Otp.Where(i => i.Mobile == otp.Mobile && i.Otp == otp.Otp
                                      && i.Imei == otp.Imei && i.IsExpired == false).FirstOrDefault();
            if (otpData != null)
                UpdateOTP(otpData.Id);

            return otpData;
        }

        private void UpdateOTP(int id)
        {
            var otp = _context.Otp.Find(id);

            if (otp == null)
                throw new AppException("Invalid OTP");

            otp.IsExpired = true;
            _context.Otp.Update(otp);
            _context.SaveChanges();
        }

        public User Create(User user)
        {
            // validation
            //if (string.IsNullOrWhiteSpace(password))
            //    throw new AppException("Password is required");

            if (_context.Users.Any(x => x.Mobile == user.Mobile))
                throw new AppException("Username \"" + user.Mobile + "\" is already Exist");
            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public OTP GenrateOtp(OTP otp)
        {
            // validation
            if (string.IsNullOrWhiteSpace(otp.Mobile))
                throw new AppException("Mobile is required");

            if (string.IsNullOrWhiteSpace(otp.Imei))
                throw new AppException("IMEI is required");

            Random r = new Random();
            string OTP = r.Next(100000, 999999).ToString();
            var key = _appSettings.SMSAPIKey;

            SMSHelper sms = new SMSHelper();
            sms.Mobile = otp.Mobile;
            var response = sms.SendOtp(OTP);

            if (response == null || response.ErrorCode != "000")
                throw new AppException(response == null ? "Something went wrong at SMS gateway hub" : "SMS Gateway Hub " + response.ErrorMessage);

            otp.Otp = Convert.ToInt32(OTP);

            _context.Otp.Add(otp);
            _context.SaveChanges();

            return otp;
        }

        public void Update(User userParam, string password = null)
        {
            var user = _context.Users.Find(userParam.Id);

            if (user == null)
                throw new AppException("User not found");

            // update username if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.Mobile) && userParam.Mobile != user.Mobile)
            {
                // throw error if the new username is already taken
                if (_context.Users.Any(x => x.Mobile == userParam.Mobile))
                    throw new AppException("User with same number " + userParam.Mobile + " is already taken");

                user.Mobile = userParam.Mobile;
            }

            // update user properties if provided
            if (!string.IsNullOrWhiteSpace(userParam.FirstName))
                user.FirstName = userParam.FirstName;

            if (!string.IsNullOrWhiteSpace(userParam.LastName))
                user.LastName = userParam.LastName;

            // update password if provided
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

               // user.PasswordHash = passwordHash;
               // user.PasswordSalt = passwordSalt;
            }

            _context.Users.Update(user);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = _context.Users.Find(id);
            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
            }
        }

        // private helper methods

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
    }

}
