﻿using InviteMe.Helpers;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InviteMe.Migrations.SqliteMigrations
{
    [DbContext(typeof(SqliteDataContext))]
    [Migration("20200102102942_InitialCreate")]
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: false),
                    DisplayPicture = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    ModifiedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OTP",
                 columns: table => new
                 {
                     Id = table.Column<int>(nullable: false)
                     .Annotation("Sqlite:Autoincrement", true),
                     Mobile = table.Column<string>(nullable: false),
                     Otp = table.Column<int>(nullable: false),
                     Imei = table.Column<string>(nullable: false),
                     IsExpired = table.Column<bool>(nullable: false, defaultValue: false),
                     CreatedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                 },
             constraints: table =>
             {
                 table.PrimaryKey("PK_Otp", x => x.Id);
             });

            migrationBuilder.CreateTable(
                name: "DeviceInfo",
                 columns: table => new
                 {
                     Id = table.Column<long>(nullable: false)
                     .Annotation("Sqlite:Autoincrement", true),
                     DeviceId = table.Column<long>(nullable: false),
                     Uid = table.Column<long>(nullable: false),
                     IMEI = table.Column<string>(nullable: false),
                     IMSI = table.Column<string>(nullable: true),
                     NetworkProvider = table.Column<string>(nullable: false),
                     OsVersion = table.Column<string>(nullable: false),
                     APILevel = table.Column<string>(nullable: true),
                     Product = table.Column<string>(nullable: false),
                     Model = table.Column<string>(nullable: false),
                     Brand = table.Column<string>(nullable: true),
                     BootLoader = table.Column<string>(nullable: true),
                     Board = table.Column<string>(nullable: true),
                     Hardware = table.Column<string>(nullable: true),
                     Host = table.Column<string>(nullable: true),
                     Manufacturer = table.Column<string>(nullable: true),
                     UserId = table.Column<string>(nullable: false),
                     Serial = table.Column<string>(nullable: true),
                     IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                     ModifiedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                 },
             constraints: table =>
             {
                 table.PrimaryKey("PK_DeviceInfo", x => x.Id);
                 table.ForeignKey("FK_DeviceInfo_Users_UserId", x => x.Uid, "Users", "Id", onDelete: ReferentialAction.NoAction);
             });

            migrationBuilder.CreateTable(
                name: "CeremonyType",
                 columns: table => new
                 {
                     Id = table.Column<long>(nullable: false)
                     .Annotation("Sqlite:Autoincrement", true),
                     Name = table.Column<string>(nullable: false),
                     IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                     ModifiedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                 },
             constraints: table =>
             {
                 table.PrimaryKey("PK_CeremonyType", x => x.Id);
             });

            migrationBuilder.CreateTable(
                name: "EventType",
                 columns: table => new
                 {
                     Id = table.Column<long>(nullable: false)
                     .Annotation("Sqlite:Autoincrement", true),
                     Name = table.Column<string>(nullable: false),
                     IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                     ModifiedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                 },
             constraints: table =>
             {
                 table.PrimaryKey("PK_EventType", x => x.Id);
             });

            migrationBuilder.CreateTable(
                name: "Template",
                 columns: table => new
                 {
                     Id = table.Column<long>(nullable: false)
                     .Annotation("Sqlite:Autoincrement", true),
                     Name = table.Column<string>(nullable: false),
                     Html = table.Column<string>(nullable: false),
                     IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                     ModifiedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                 },
             constraints: table =>
             {
                 table.PrimaryKey("PK_Template", x => x.Id);
             });


            migrationBuilder.CreateTable(
                name: "Ceremony",
                 columns: table => new
                 {
                     Id = table.Column<long>(nullable: false)
                     .Annotation("Sqlite:Autoincrement", true),
                     Name = table.Column<string>(nullable: false),
                     CeremonyTypeId = table.Column<long>(nullable: false),
                     UserId = table.Column<long>(nullable: false),
                     Address = table.Column<string>(nullable: false),
                     Latitude = table.Column<double>(nullable: false),
                     Longitude = table.Column<double>(nullable: false),
                     Descrition = table.Column<string>(nullable: false),
                     OnDate = table.Column<DateTime>(nullable: false),
                     IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                     ModifiedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                 },
             constraints: table =>
             {
                 table.PrimaryKey("PK_Ceremony", x => x.Id);
                 table.ForeignKey("FK_Ceremony_Users_UserId", x => x.UserId, "Users", "Id", onDelete: ReferentialAction.NoAction);
                 table.ForeignKey("FK_Ceremony_CeremonyType_CeremonyTypeId", x => x.CeremonyTypeId, "CeremonyType", "Id", onDelete: ReferentialAction.NoAction);

             });

            migrationBuilder.CreateTable(
                name: "Event",
                 columns: table => new
                 {
                     Id = table.Column<long>(nullable: false)
                     .Annotation("Sqlite:Autoincrement", true),
                     Name = table.Column<string>(nullable: false),
                     CeremonyId = table.Column<long>(nullable: false),
                     EventCategoryId = table.Column<long>(nullable: false),
                     Address = table.Column<string>(nullable: false),
                     Latitude = table.Column<double>(nullable: false),
                     Longitude = table.Column<double>(nullable: false),
                     Descrition = table.Column<string>(nullable: false),
                     OnDate = table.Column<DateTime>(nullable: false),
                     IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                     ModifiedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                 },
             constraints: table =>
             {
                 table.PrimaryKey("PK_Event", x => x.Id);
                 table.ForeignKey("FK_Event_EventType_EventCategoryId", x => x.EventCategoryId, "EventType", "Id", onDelete: ReferentialAction.NoAction);
                 table.ForeignKey("FK_Event_Ceremony_CeremonyId", x => x.CeremonyId, "Ceremony", "Id", onDelete: ReferentialAction.NoAction);
             });

            migrationBuilder.CreateTable(
                name: "Attendes",
                 columns: table => new
                 {
                     Id = table.Column<long>(nullable: false)
                     .Annotation("Sqlite:Autoincrement", true),
                     CeremonyId = table.Column<long>(nullable: false),
                     EventId = table.Column<long>(nullable: false),
                     Html = table.Column<string>(nullable: false),
                     Email = table.Column<double>(nullable: false),
                     Mobile = table.Column<double>(nullable: false),
                     MessageStatus = table.Column<string>(nullable: false),
                     IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                     ModifiedOn = table.Column<DateTime>(nullable: false, defaultValue: DateTime.Now)
                 },
             constraints: table =>
             {
                 table.PrimaryKey("PK_Attendes", x => x.Id);
                 table.ForeignKey("FK_Attendes_Event_EventId", x => x.EventId, "Event", "Id", onDelete: ReferentialAction.NoAction);
                 table.ForeignKey("FK_Attendes_Ceremony_CeremonyId", x => x.CeremonyId, "Ceremony", "Id", onDelete: ReferentialAction.NoAction);
             });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }

}
