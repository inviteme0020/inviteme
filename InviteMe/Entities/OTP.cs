﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InviteMe.Entities
{
    public class OTP
    {
        public int Id { get; set; }
        public string Mobile { get; set; }
        public string Imei { get; set; }
        public int Otp { get; set; }
        public bool IsExpired { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
