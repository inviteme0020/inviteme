﻿using InviteMe.Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InviteMe.Helpers
{
    public class SMSHelper
    {
        private const string sAPIKey = "r8i7RRXdoUqCn5gT0DFskg";
        private const string sSenderID = "BIGSOP";
        string sChannel = "2";
        string sDCS = "0";
        string sFlashsms = "0";
        public string Mobile { get; set; }

        public SMSGatewayResponse SendOtp(string otp)
        {
            string sMessage = $"OTP for your InviteMe app is {otp}. This OTP is valid for one trxn. or 30 mins only. Do not share it with anyone";
            string sURL = "https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=" + sAPIKey + "&senderid=" + sSenderID + "&channel=" + sChannel + "&DCS=" + sDCS + "&flashsms=" + sFlashsms + "&number=" + Mobile + "&text=" + sMessage + "&route=1";
            var response = GetResponse(sURL);
            return JsonConvert.DeserializeObject<SMSGatewayResponse>(response);
        }

        public static string GetResponse(string sURL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);

            request.MaximumAutomaticRedirections = 4;

            request.Credentials = CredentialCache.DefaultCredentials;

            try
            {

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream receiveStream = response.GetResponseStream();

                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                string sResponse = readStream.ReadToEnd();

                response.Close();

                readStream.Close();

                return sResponse;

            }
            catch
            {
                return "";
            }

        }
    }
}
