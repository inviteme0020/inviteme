﻿using AutoMapper.Configuration;
using InviteMe.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace InviteMe.Helpers
{
    public class DataContext : DbContext
    {
        protected readonly Microsoft.Extensions.Configuration.IConfiguration Configuration;

        public DataContext(Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to sql server database
            options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
        }

        public DbSet<User> Users { get; set; }

        public DbSet<OTP> Otp { get; set; }
    }

}
