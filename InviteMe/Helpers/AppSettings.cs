﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InviteMe.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string SMSAPIKey { get; set; }
    }
}
