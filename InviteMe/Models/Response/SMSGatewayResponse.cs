﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InviteMe.Models.Response
{
    public class SMSGatewayResponse
    {
        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }
        public string Number { get; set; }

    }
}
