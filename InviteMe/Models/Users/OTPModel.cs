﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InviteMe.Models.Users
{
    public class OTPModel
    {
        public int Otp { get; set; }
        public string Mobile { get; set; }
        public string Imei { get; set; }
    }
}
